from fabric.api import run, env, task, parallel, put, execute, cd, sudo, get
from fabric.context_managers import shell_env
import socket
import paramiko
import time
# Python Fabric script to run commands on multiple hosts through ssh
#
# Run script as 'fab <task>', where <task> is one of the scripts functions
# marked as a tesk. The task marked as 'default' will be run if <task> is not
# specified
#
# Note that the execute-operation is needed when working this dynamic lists
# of hosts

#env.hosts= ['sdn02.lab.es.aau.dk', 'sdn09.lab.es.aau.dk', 'sdn10.lab.es.aau.dk', 'sdn11.lab.es.aau.dk' ]
env.hosts= ['sdn02.lab.es.aau.dk', 'sdn12.lab.es.aau.dk'  ]

env.user = 'root'
env.password = 'sdn'

# The dafault task. If others are using the testbed then DO NOT use the
# prepare_hosts method. Just use the global env.hosts and set it as the hosts in
# the execution call.
@task(default=True)
@parallel
def setup():
    #host_list = prepare_hosts()
    #execute(prepare_sdn_testbed, hosts=env.hosts)
    prepare_sdn_testbed()

# Methods checks is the given hosts are online. If not the setup of the hosts
# is aborted.
@task
@parallel
def prepare_sdn_testbed():
    if df_h() is True:
        complete_setup_and_run()

# Run a number of commands to setup all hosts.
# Internet and disk is set. Applications are updated and needed applications are
# installed. The files needed for benchmarks are moved over ssh.
@parallel
def complete_setup_and_run():
    setup_dns()
    setup_format()
    setup_apt_update()
    setup_apt_get_gcc()
    setup_perftool()
    run('pwd')
    transfer_tar()
    setup_screen()

#Dynamically prepares the hostlist. Can easily be modified if there is more
#than 16 machines in the testbed.
def prepare_hosts():
    host_list = []

    for i in range(1,17):
        host_list.append('sdn{:0>2d}.lab.es.aau.dk'.format(i))

    return host_list

# Checks is a host is up.
# Taken from http://stackoverflow.com/questions/1956777/how-to-make-fabric-ignore-offline-hosts-in-the-env-hosts-list
def df_h():
        result = _is_host_up(env.host, int(env.port))
        return result

# Helper method to check is host is up.
# Also taken from http://stackoverflow.com/questions/1956777/how-to-make-fabric-ignore-offline-hosts-in-the-env-hosts-list
def _is_host_up(host, port):
    # Set the timeout
    original_timeout = socket.getdefaulttimeout()
    new_timeout = 3
    socket.setdefaulttimeout(new_timeout)
    host_status = False
    try:
        transport = paramiko.Transport((host, port))
        host_status = True
    except:
        print('***Warning*** Host {host} on port {port} is down.'.format(
            host=host, port=port)
        )
        #env.hosts.remove(host)
    socket.setdefaulttimeout(original_timeout)
    return host_status


# Setup DNS on all hosts such that they all have internet access
def setup_dns():
    result = run('/scripts/set_google_dns.sh', quiet=True)

# Mounts the sdn disk
# Call must be root. Otherwise the file can't be found
# Call must include bash. Otherwise it will not execute.
def setup_format():
    result = sudo('bash /scripts/formating.sh', quiet=True)

# Update the apt-get repo
def setup_apt_update():
    result = run('apt-get update --force-yes', quiet=True)

# Updates gcc so the benchmarks have de needed libraries.
def setup_apt_get_gcc():
    result = run('DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes gcc', quiet=True)

# Install perftool for profiling
def setup_perftool():
    result = run('apt-get install -y google-perftools', quiet=True)

# Installs screen and run benchmarks
# @ToDo Needs to put this out in to methods.
def setup_screen():
    result = run('apt-get install -y screen', quiet=True)
    with cd('../home/sdn/test'):
        result = sudo('screen -d -m -S \"test_screen\" ./screen_setup.sh')

# Sends tar.gz over ssh and uncompress' it.
def transfer_tar():
    with cd('../home/sdn'):
        result = put('test.tar.gz', '.')
        result = run('tar -zxvf test.tar.gz')

# Run benchmarks.
# Not used at the moment.
def run_benchmarks():
    with cd('../home/sdn'):
        result = run('cd tests; mkdir results;  python testbed_script.py', quiet=True)

@task
@parallel
def get_results():
    with cd('../home/sdn/test'):
        get('decoder_probability.json')
        get('throughput.json')
        sudo('tar -zcvf results.tar.gz results', quiet=True)
        get('results.tar.gz')

def results_manually():
        seed = ['with', 'without']
        with cd('results'):
            symbols = 8
            size = 50
            while symbols <=1500:
                while size <= 1500:
                    for s in seed:
                        get(str(symbols)
                            + '_'
                            + str(size)
                            + '_'
                            + s
                            + '.prof')
                    size = size + 50
                symbols = symbols + 8
                size = 50

@task
#Reboots all machines
def reboot_testbed():
    #host_list = prepare_hosts()
    #execute(reboot, hosts=host_list)
    reboot()

@parallel
def reboot():
    if df_h() is True:
        run('reboot', quiet=True)
