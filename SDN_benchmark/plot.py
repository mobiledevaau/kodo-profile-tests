import os
from os.path import basename
import re
import subprocess as sp
from pandas import *
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages



def get_test_folder():
    folders = [folder[0] for folder in os.walk(os.getcwd())]
    test_folders = []

    #Regex to make sure only the currect folders are chosen
    sdn_regex = re.compile('sdn\d\d.lab.es.aau.dk$')

    for f in folders:
        if sdn_regex.search(f):
            test_folders.append(f)

    return test_folders

def extract_tar(test_folders):
    for f in test_folders:
        p = sp.Popen(['tar -zxvf results.tar.gz'], shell=True, cwd=f)
        p.wait()


def testcase_mapper(test, seed):
    if seed == True:
        return test + '_with'
    else:
        return test + '_without'

def convertThoughputToDF(json_file):
    df = read_json(json_file)
    df = df.set_index(['benchmark', 'symbols', 'symbol_size', 'testcase']).sort_index()
    df = df.drop(['goodput', 'iterations', 'type', 'unit', 'run_number'], axis=1)
    df['throughput'] = df['throughput'].apply(np.mean)

    ## Settings labels for new dataframe ##
    old_labels = df.index.values.tolist()

    binaries = list(set([i[0] for i in old_labels]))
    binaries.sort()

    symbols = list(set([i[1] for i in old_labels]))
    symbols.sort()

    symbol_size = list(set([i[2] for i in old_labels]))
    symbol_size.sort()

    testcases = list(set([i[3] for i in old_labels]))
    testcases.sort()

    label_list = [binaries, symbols, symbol_size]

    index = MultiIndex.from_product(label_list, names=['Binary', 'symbols', 'symbol_size'])
    columns = testcases

    df2 = DataFrame(columns=columns, index=index)

    #Fills the new DF with data
    for binary in binaries:
        for symbol in symbols:
            for size in symbol_size:
                for test in testcases:
                    result = df.loc[binary, symbol, size, test][0] #Type is a pd.Series so the first index must be taken
                    df2.loc[binary, symbol, size][test] = result

    return df2

def convertDecoderToDF(json_file):
    df = read_json(json_file)
    df = df.set_index(['benchmark', 'symbols', 'symbol_size', 'testcase', 'seed']).sort_index()
    df = df.drop(['iterations', 'erasure', 'unit', 'run_number', 'rank', 'systematic'], axis=1)
    df['used'] = df['used'].apply(np.mean)

    ## Settings labels for new dataframe ##
    old_labels = df.index.values.tolist()

    binaries = list(set([i[0] for i in old_labels]))
    binaries.sort()

    symbols = list(set([i[1] for i in old_labels]))
    symbols.sort()

    symbol_size = list(set([i[2] for i in old_labels]))
    symbol_size.sort()

    testcases = list(set([i[3] for i in old_labels]))
    testcases.sort()

    seeds = list(set([i[4] for i in old_labels]))
    seeds.sort()

    label_list = [binaries, symbols, symbol_size]

    index = MultiIndex.from_product(label_list, names=['Binary', 'symbols', 'symbol_size'])
    columns = []

    for test in testcases:
        for seed in seeds:
            case = testcase_mapper(test, seed)
            columns.append(case)

    df2 = DataFrame(columns=columns, index=index)

    #Fills the new DF with data
    for binary in binaries:
        for symbol in symbols:
            for size in symbol_size:
                for test in testcases:
                    for seed in seeds:
                        result = df.loc[binary, symbol, size, test, seed][0] #Type is a pd.Series so the first index must be taken
                        case = testcase_mapper(test, seed)
                        df2.loc[binary, symbol, size][case] = result

    return df2

def convertProfToCSV(folder):
    seeds = ['with', 'without']
    for seed in seeds:
        csv_file = folder + '/results_' + seed + '.csv'
        #print csv_file

        new_file = open(csv_file, 'w')
        new_file.write('Gen_size,max_symbol_size,time_generator,time_fifi,time_seeding\n')
        #print folder
        #break
        files = os.listdir(folder + '/results')
        prof_folder = folder + '/results'

        regex_file = re.compile( seed + '.prof')
        regex_1 = re.compile('[\d|\s]\d.\d(?=% kodo::uniform_generator::generate)')
        regex_2 = re.compile('[\d|\s]\d.\d(?=% kodo::finite_field_math)')
        regex_3 = re.compile('[\d|\s]\d.\d(?=% kodo::seed_generator)')

        seed_files = []

        for data_file in files:
            if regex_file.search(basename(data_file)):
                seed_files.append(data_file)

        # Uses regex to find the the data to include in the csv.
        for data_file in seed_files:
            call_string = 'google-pprof --text encode_benchmark ' + prof_folder + '/' + basename(data_file) + ' > tmp.txt'
            sp.call([call_string], shell=True)

            prof = 'tmp.txt'
            prof_file = open(prof, 'r')
            dat_1 = '0'
            dat_2 = '0'
            dat_3 = '0'
            for f in prof_file:
                if regex_1.search(f):
                    dat_1 = regex_1.search(f).group()
                if regex_2.search(f):
                    dat_2 = regex_2.search(f).group()
                if regex_3.search(f):
                    dat_3 = regex_3.search(f).group()

            gen = basename(data_file).split('.')[0].split('_')[0]
            max_size = basename(data_file).split('.')[0].split('_')[1]
            new_file.write(gen + ',' + max_size  + ',' + dat_1 + ',' + dat_2 + ',' + dat_3 + '\n')
            prof_file.close()

        new_file.close()
        os.remove('tmp.txt')

def dfs_by_test(test_folders, test):
    dfs = []
    for f in test_folders:
        if test == 'decoder':
            dfs.append(convertDecoderToDF(f + '/decoder_probability.json'))
        elif test == 'throughput':
            dfs.append(convertThoughputToDF(f + '/throughput.json'))

    old_labels = dfs[0].index.values.tolist()

    binaries = list(set([i[0] for i in old_labels]))
    binaries.sort()

    symbols = list(set([i[1] for i in old_labels]))
    symbols.sort()

    symbol_size = list(set([i[2] for i in old_labels]))
    symbol_size.sort()

    testcases = dfs[0].columns.values.tolist()
    testcases.sort()

    label_list = [binaries, symbols, symbol_size]

    index = MultiIndex.from_product(label_list, names=['Binary', 'symbols', 'symbol_size'])

    new_df = DataFrame(columns = dfs[0].columns.tolist(), index = index)

    #Fills the new DF with
    for binary in binaries:
        for symbol in symbols:
            for size in symbol_size:
                for test in testcases:
                    result = []
                    for df in dfs:
                        result.append(df.loc[binary, symbol, size][test])
                    new_df.loc[binary, symbol, size][test] = result

    return new_df


def plot(test_folders):

    encoder_dfs = dfs_by_test(test_folders, 'decoder')
    throughput_dfs = dfs_by_test(test_folders, 'throughput')

    encoder_tests = encoder_dfs.columns.values.tolist()
    encoder_mean = encoder_dfs.copy()
    encoder_std = encoder_dfs.copy()
    for test in encoder_tests:
        encoder_mean[test] = encoder_dfs[test].apply(np.mean)
        encoder_std[test] = encoder_dfs[test].apply(np.std)

    throughput_tests = throughput_dfs.columns.values.tolist()
    throughput_mean = throughput_dfs.copy()
    throughput_std = throughput_dfs.copy()
    for test in throughput_tests:
        throughput_mean[test] = throughput_dfs[test].apply(np.mean)
        throughput_std[test] = throughput_dfs[test].apply(np.std)

    ## Getting values to iterate through##
    old_labels = encoder_dfs.index.values.tolist()

    binaries = list(set([i[0] for i in old_labels]))
    binaries.sort()

    symbols = list(set([i[1] for i in old_labels]))
    symbols.sort()

    symbol_size = list(set([i[2] for i in old_labels]))
    symbol_size.sort()



    for binary in binaries:
        with PdfPages('results_' + binary + '.pdf') as pdf:
            for symbol in symbols:
                fig, axis = plt.subplots(nrows=3,
                                         ncols=1,
                                         figsize=(12,8),
                                         squeeze=False)

                encoder_ax = encoder_mean.loc[binary, symbol].plot(ax = axis[0,0],
                                                                   kind='bar',
                                                                   sharex=False,
                                                                   title = 'Encoding probability for generation size ' + str(symbol),
                                                                   yerr = encoder_std.loc[binary, symbol])
                encoder_ax.set_ylabel('Average symbols needed')

                throughput_ax = throughput_mean.loc[binary, symbol].plot(ax = axis[1,0],
                                                                         kind='bar',
                                                                         sharex=False,
                                                                         title = 'Throughput for generation size ' + str(symbol),
                                                                         yerr = throughput_std.loc[binary, symbol])
                throughput_ax.set_ylabel('MB/s')



                plt.tight_layout()
                pdf.savefig()
                plt.close()



def main():
    #Getting the folder with results
    test_folders = get_test_folder()

    #Extracting the prof-files of the profiling
    #extract_tar(test_folders)

    plot(test_folders)

if __name__ == '__main__':
    main()
