import subprocess as sp
import os
import socket
import pexpect as px
import shutil

symbols = []
symbol_size = []
symbols_str = ''
symbol_size_str = ''


# Prepares the list of symbols. Assumes that a range is sweeped for results
def prepare_symbols():
    i = 8
    while i <= 1500:
        symbols.append(i)
        i = i + 8

    symbols_str = str(symbols).translate(None, '[]').translate(None, ',')

# Prepares the list of symbol sizes. Assumes that a range is sweeped for results
def prepare_symbol_size():
    i = 50
    while i <= 1500:
        symbol_size.append(i)
        i = i + 50

    symbol_size_str = str(symbol_size).translate(None, '[]').translate(None, ',')

# Calls the shell to run the decoder probability benchmark.
def run_decoder():
    print 'Decoder started'
    sp.call(["./kodo_decoding_probability --systematic false --gauge_filter FullRLNC.Binary FullRLNC.Binary8 --symbols " + str(symbols).translate(None, '[]').translate(None, ',') +" --symbol_size " + str(symbol_size).translate(None, '[]').translate(None, ',') + " --json_file decoder_probability.json --runs=100000"], shell=True)

# Call the shell to run the throughput benchamrks.
def run_throughput():
    sp.call(["./kodo_throughput --gauge_filter=Full_FullRLNC.Binary Full_FullRLNC.Binary8 FullRLNC.Binary FullRLNC.Binary8 --symbols " + str(symbols).translate(None, '[]').translate(None, ',') + " --symbol_size " + str(symbol_size).translate(None, '[]').translate(None, ',')  + " --type=encoder --runs=100000 --json_file=throughput.json"], shell=True)


# Call the shell to makes profiles.
# Uses the grobal lists of symbols and symbol sizes.
def run_encode():
    seed = ['with', 'without']

    for sym in symbols:
        for sym_size in symbol_size:
            for se in seed:
                sp.call(["LD_PRELOAD=libprofiler.so.0 "
                         + " CPUPROFILE=out.prof "
                         + "./encode_benchmark "
                         + str(sym) + " " + str(sym_size) + " " + str(100000) + " " + str(se) ], shell=True)

                shutil.move(
                    'out.prof',
                    'results' + '/' + str(sym) + '_' + str(sym_size) + '_' + str(se) + '.prof')


def run_tests():
    prepare_symbols()
    prepare_symbol_size()
    run_decoder()
    run_throughput()
    run_encode()

if __name__ == "__main__":
    run_tests()
