# README #

Purpose
=======
This folder contains a fabfile, a script the unilize the Fabric module for python. The script execute a series of benchmarks on several SDNs in a testbed setup via SSH. To use this script one must insert he or her own hostnames and passwords.

Installation
============
In other to execute the fabfile onw must install the Fabric module by either
```
pip install fabric
```
or
```
sudo apt-get install fabric
```

Advance install options can be found [here](http://www.fabfile.org/installing.html).


fabfile setup
=============
The fabfile is used to setup the SDN testbed and install the needed packages. All that is install is specific for this testbed and will surely be different than others.But every tool has it function calles `setup_*()` if one will change or add setup functions.

Dynamic or static hostlist
--------------------------
When executing the fabfile a hostlist can be hardcoded or it can be found dynamically. At the moment a global variable `env.hosts` is set which is how it should be done is one knows which machine there can be used.

In the default tast in the fabfile the functions `prepare_hosts()` and `execute` is commented out. These functions can be used if one have several hosts but one does not know which are online. The `prepare_hosts()` do only prepare the string for hosts. The `prepare_sdn_testbed()` checks is the host is online and if not the setup is aborted for that host.

Screen
------
Screen is used for the tests since one can detach shell created by screen and then later attach to it again. This is great since the tests are done over SSH and would otherwise terminate if the SSH connection was lost. A tutorial for sceen can be found [here](https://bitbucket.org/mobiledevaau/mobiledevdocs/wiki/SDN_Testbed#markdown-header-screen).

Parallel execution
------------------
Some of the functions defined in the fabfile are `@parallel`. This means that if the function must be executed on more than one hosts then the function is done in parallel on the hosts. An example is the setup of the testbed. This can be done in parallel on the machines since the machines does not interact.

Transfer tests
--------------
When running tests on the SDN a `transfer_tar()` function is defined. This makes it possible to transfer all files at once to the SDN. One must by him or herself compress the files needed into a tar. The name of the tar is hardcoded to be `test.tar.gz`.

testbed\_script
===============
The fabfile job is to setup the SDNs for running a series of tests while it is *testbed\_script* responsible to tell what tests shall be run, with which settings and so on. At the moment the tests are two benchmarks from the Kodo Library:
- Decoder probability. How many packets are needed for decoding.
- Throughput. The throughput of the encoder.
- Encoding profiler. The profile of the encoder.

The folder *test* contains the file *testbed\_script* and the executables that was used for the tests. 