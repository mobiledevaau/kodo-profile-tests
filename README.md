# README #

Purpose
=========
This is the readme for the kodo-profile-test repo. This repo is for test and benchmarks for measuring performange of the Kodo library. Kodo itself have benchmarks but these are very demo oriented where those in this repo are more developer minded.

Tests/Benchmarks
==================
All folders contains a README.txt of what the test is for, how to execute, who to understand the data and what else might be usefull.

Tests in the repo:

- Encoding profiling. Tests the amount of time of total spend in finite field math, seeding the random number generator and in generating random numbers. Contains script for plotting the results.
- Encoding performance measurement counting. A test that reads counters of the processor for getting the cache miss ratio.
- Generator benchmarks. These benchmarks shows the speed of different Pseudo Random Number Generators under certain conditions.
- SDN benchmarks. Description of have a SDN testbed was used for profiling and benchmarking.


ALL BELOW TO BE REMOVED!!
About
=====
This repositiry contains different and hotspot tests of kodo.


Build
=====
To configure the tests:

```
#!python
python waf configure
```

This will configure the project and download all the dependencies needed.

After configure run the following command to build:

```
#!python
python waf build
```


If you experience problems with either of the two previous steps you can
check the Kodo manual's `Getting Started`_ section for trubleshooting.

[Getting Started](https://kodo.readthedocs.org/en/latest/getting_started.html)

Run
===

After building you have a couple of different tests. The different test are in the folder:

```
#!python
./build/linux
```
E.g. the encoding profiler can be started by typing

```
#!python
./build/linux/encoding_profiler
```

You can run the test with the ``--help`` option to see which parameters
are supported:
```
#!python
./build/linux/encoding_profiler --help
```
