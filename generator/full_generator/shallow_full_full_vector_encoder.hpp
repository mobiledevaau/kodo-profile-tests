// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#pragma once

#include "../coefficient_info.hpp"
#include "../coefficient_value_access.hpp"
#include "../default_on_systematic_encoder.hpp"
#include "../final_layer.hpp"
#include "../finite_field_layers.hpp"
#include "../has_shallow_symbol_storage.hpp"
#include "../linear_block_encoder.hpp"
#include "../partial_const_shallow_storage_layers.hpp"
#include "../payload_info.hpp"
#include "../plain_symbol_id_writer.hpp"
#include "../pool_factory.hpp"
#include "../shallow_symbol_storage.hpp"
#include "../storage_aware_encoder.hpp"
#include "../symbol_id_encoder.hpp"
#include "../trace_layer.hpp"
#include "../uniform_full_generator_layers.hpp"
#include "../write_symbol_tracker.hpp"
#include "../zero_symbol_encoder.hpp"
#include "../common_encoder_layers.hpp"

namespace kodo
{
namespace rlnc
{
    /// @ingroup fec_stacks
    ///
    /// @brief Complete stack implementing a shallow storage RLNC encoder.
    ///
    /// The encoder is identical to the full_vector_encoder except for
    /// the fact that is uses a shallow storage layer.
    template
    <
        class Field,
        class Features = meta::typelist<>
    >
    class shallow_full_full_vector_encoder : public
        // Payload Codec API
        payload_info<
        // Codec Header API
        default_on_systematic_encoder<
        symbol_id_encoder<
        // Symbol ID API
        plain_symbol_id_writer<
        // Coefficient Generator API
        uniform_full_generator_layers<
        // Encoder API
        common_encoder_layers<Features,
        // Coefficient Storage API
        coefficient_value_access<
        coefficient_info<
        // Symbol Storage API
        partial_const_shallow_storage_layers<Features,
         // Finite Field API
        finite_field_layers<Field,
        // Trace Layer
        trace_layer<find_enable_trace<Features>,
        // Final Layer
        final_layer
        > > > > > > > > > > >
    {
    public:
        using factory = pool_factory<shallow_full_full_vector_encoder>;
    };
}
}
