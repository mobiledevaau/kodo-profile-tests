// Copyright Steinwurf ApS 2011.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#pragma once

#include "full_vector_decoder.hpp"
#include "full_vector_encoder.hpp"
#include "full_vector_recoder.hpp"

#include "shallow_full_vector_encoder.hpp"
#include "shallow_full_full_vector_encoder.hpp"
#include "shallow_full_vector_decoder.hpp"

#include "shallow_backward_full_vector_decoder.hpp"
#include "shallow_delayed_full_vector_decoder.hpp"
#include "shallow_sparse_full_vector_encoder.hpp"

#include "sparse_full_vector_encoder.hpp"
