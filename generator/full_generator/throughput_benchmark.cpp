// Copyright Steinwurf ApS 2011.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#include <ctime>

#include <gauge/gauge.hpp>
#include <gauge/console_printer.hpp>
#include <gauge/python_printer.hpp>
#include <gauge/csv_printer.hpp>
#include <gauge/json_printer.hpp>

#include <kodo/rlnc/full_vector_codes.hpp>
#include <kodo/rlnc/seed_codes.hpp>

#include "throughput_benchmark.hpp"

/// Using this macro we may specify options. For specifying options
/// we use the boost program options library. So you may additional
/// details on how to do it in the manual for that library.
BENCHMARK_OPTION(throughput_options)
{
    gauge::po::options_description options;

    std::vector<uint32_t> symbols;
    symbols.push_back(8);
    symbols.push_back(16);
    symbols.push_back(32);
    symbols.push_back(64);
    symbols.push_back(128);
    symbols.push_back(256);
    symbols.push_back(512);

    auto default_symbols =
        gauge::po::value<std::vector<uint32_t> >()->default_value(
            symbols, "")->multitoken();

    std::vector<uint32_t> symbol_size;
    symbol_size.push_back(1600);

    auto default_symbol_size =
        gauge::po::value<std::vector<uint32_t> >()->default_value(
            {1600}, "")->multitoken();

    std::vector<std::string> types;
    types.push_back("encoder");
    types.push_back("decoder");

    auto default_types =
        gauge::po::value<std::vector<std::string> >()->default_value(
            types, "")->multitoken();

    options.add_options()
        ("symbols", default_symbols, "Set the number of symbols");

    options.add_options()
        ("symbol_size", default_symbol_size, "Set the symbol size in bytes");

    options.add_options()
        ("type", default_types, "Set type [encoder|decoder]");

    gauge::runner::instance().register_options(options);
}


//------------------------------------------------------------------
// Shallow Full FullRLNC
//------------------------------------------------------------------

using setup_full_rlnc_throughput = throughput_benchmark<
    kodo::rlnc::shallow_full_full_vector_encoder<fifi::binary>,
    kodo::rlnc::shallow_full_vector_decoder<fifi::binary>>;

BENCHMARK_F_INLINE(setup_full_rlnc_throughput, Full_FullRLNC, Binary, 5)
{
    run_benchmark();
}

using setup_full_rlnc_throughput8 = throughput_benchmark<
    kodo::rlnc::shallow_full_full_vector_encoder<fifi::binary8>,
    kodo::rlnc::shallow_full_vector_decoder<fifi::binary8>>;

BENCHMARK_F_INLINE(setup_full_rlnc_throughput8, Full_FullRLNC, Binary8, 5)
{
    run_benchmark();
}

using setup_full_rlnc_throughput16 = throughput_benchmark<
    kodo::rlnc::shallow_full_full_vector_encoder<fifi::binary16>,
    kodo::rlnc::shallow_full_vector_decoder<fifi::binary16>>;

BENCHMARK_F_INLINE(setup_full_rlnc_throughput16, Full_FullRLNC, Binary16, 5)
{
    run_benchmark();
}

//------------------------------------------------------------------
// Shallow FullRLNC
//------------------------------------------------------------------

using setup_rlnc_throughput = throughput_benchmark<
    kodo::rlnc::shallow_full_vector_encoder<fifi::binary>,
    kodo::rlnc::shallow_full_vector_decoder<fifi::binary>>;

BENCHMARK_F_INLINE(setup_rlnc_throughput, FullRLNC, Binary, 5)
{
    run_benchmark();
}

using setup_rlnc_throughput8 = throughput_benchmark<
    kodo::rlnc::shallow_full_vector_encoder<fifi::binary8>,
    kodo::rlnc::shallow_full_vector_decoder<fifi::binary8>>;

BENCHMARK_F_INLINE(setup_rlnc_throughput8, FullRLNC, Binary8, 5)
{
    run_benchmark();
}

using setup_rlnc_throughput16 = throughput_benchmark<
    kodo::rlnc::shallow_full_vector_encoder<fifi::binary16>,
    kodo::rlnc::shallow_full_vector_decoder<fifi::binary16>>;

BENCHMARK_F_INLINE(setup_rlnc_throughput16, FullRLNC, Binary16, 5)
{
    run_benchmark();
}

using setup_rlnc_throughput2325 = throughput_benchmark<
    kodo::rlnc::shallow_full_vector_encoder<fifi::prime2325>,
    kodo::rlnc::shallow_full_vector_decoder<fifi::prime2325>>;

BENCHMARK_F_INLINE(setup_rlnc_throughput2325, FullRLNC, Prime2325, 5)
{
    run_benchmark();
}

//------------------------------------------------------------------
// Shallow SeedRLNC
//------------------------------------------------------------------

using setup_seed_rlnc_throughput = throughput_benchmark<
    kodo::rlnc::shallow_seed_encoder<fifi::binary>,
    kodo::rlnc::shallow_seed_decoder<fifi::binary>>;

BENCHMARK_F_INLINE(setup_seed_rlnc_throughput, SeedRLNC, Binary, 5)
{
    run_benchmark();
}

using setup_seed_rlnc_throughput8 = throughput_benchmark<
    kodo::rlnc::shallow_seed_encoder<fifi::binary8>,
    kodo::rlnc::shallow_seed_decoder<fifi::binary8>>;

BENCHMARK_F_INLINE(setup_seed_rlnc_throughput8, SeedRLNC, Binary8, 5)
{
    run_benchmark();
}

using setup_seed_rlnc_throughput16 = throughput_benchmark<
    kodo::rlnc::shallow_seed_encoder<fifi::binary16>,
    kodo::rlnc::shallow_seed_decoder<fifi::binary16>>;

BENCHMARK_F_INLINE(setup_seed_rlnc_throughput16, SeedRLNC, Binary16, 5)
{
    run_benchmark();
}

//------------------------------------------------------------------
// Shallow BackwardFullRLNC
//------------------------------------------------------------------

using setup_backward_rlnc_throughput = throughput_benchmark<
    kodo::rlnc::shallow_full_vector_encoder<fifi::binary>,
    kodo::rlnc::shallow_backward_full_vector_decoder<fifi::binary> >;

BENCHMARK_F_INLINE(setup_backward_rlnc_throughput, BackwardFullRLNC,
                   Binary, 5)
{
    run_benchmark();
}

using setup_backward_rlnc_throughput8 = throughput_benchmark<
    kodo::rlnc::shallow_full_vector_encoder<fifi::binary8>,
    kodo::rlnc::shallow_backward_full_vector_decoder<fifi::binary8> >;

BENCHMARK_F_INLINE(setup_backward_rlnc_throughput8, BackwardFullRLNC,
                   Binary8, 5)
{
    run_benchmark();
}

using setup_backward_rlnc_throughput16 = throughput_benchmark<
    kodo::rlnc::shallow_full_vector_encoder<fifi::binary16>,
    kodo::rlnc::shallow_backward_full_vector_decoder<fifi::binary16> >;

BENCHMARK_F_INLINE(setup_backward_rlnc_throughput16, BackwardFullRLNC,
                   Binary16, 5)
{
    run_benchmark();
}

using setup_backward_rlnc_throughput2325 = throughput_benchmark<
    kodo::rlnc::shallow_full_vector_encoder<fifi::prime2325>,
    kodo::rlnc::shallow_backward_full_vector_decoder<fifi::prime2325> >;

BENCHMARK_F_INLINE(setup_backward_rlnc_throughput2325, BackwardFullRLNC,
                   Prime2325, 5)
{
    run_benchmark();
}

//------------------------------------------------------------------
// Shallow FullDelayedRLNC
//------------------------------------------------------------------

using setup_delayed_rlnc_throughput = throughput_benchmark<
   kodo::rlnc::shallow_full_vector_encoder<fifi::binary>,
   kodo::rlnc::shallow_delayed_full_vector_decoder<fifi::binary> >;

BENCHMARK_F_INLINE(setup_delayed_rlnc_throughput, FullDelayedRLNC,
                   Binary, 5)
{
   run_benchmark();
}

using setup_delayed_rlnc_throughput8 = throughput_benchmark<
   kodo::rlnc::shallow_full_vector_encoder<fifi::binary8>,
   kodo::rlnc::shallow_delayed_full_vector_decoder<fifi::binary8> >;

BENCHMARK_F_INLINE(setup_delayed_rlnc_throughput8, FullDelayedRLNC,
                   Binary8, 5)
{
   run_benchmark();
}

using setup_delayed_rlnc_throughput16 = throughput_benchmark<
   kodo::rlnc::shallow_full_vector_encoder<fifi::binary16>,
   kodo::rlnc::shallow_delayed_full_vector_decoder<fifi::binary16> >;

BENCHMARK_F_INLINE(setup_delayed_rlnc_throughput16, FullDelayedRLNC,
                   Binary16, 5)
{
   run_benchmark();
}

using setup_delayed_rlnc_throughput2325 = throughput_benchmark<
   kodo::rlnc::shallow_full_vector_encoder<fifi::prime2325>,
   kodo::rlnc::shallow_delayed_full_vector_decoder<fifi::prime2325> >;

BENCHMARK_F_INLINE(setup_delayed_rlnc_throughput2325, FullDelayedRLNC,
                   Prime2325, 5)
{
   run_benchmark();
}

int main(int argc, const char* argv[])
{
    srand(static_cast<uint32_t>(time(0)));

    gauge::runner::add_default_printers();
    gauge::runner::run_benchmarks(argc, argv);

    return 0;
}
