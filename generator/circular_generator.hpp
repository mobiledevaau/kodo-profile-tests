// Copyright Steinwurf ApS 2015
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing
#pragma once

#include <cstdint>
#include <boost/random/mersenne_twister.hpp> //mt11213b and mt19937
#include <boost/random/shuffle_order.hpp> //knuth_b and kreutzer1986
#include <boost/random/taus88.hpp> //taus88
#include <boost/random/uniform_int_distribution.hpp>
#include <kodo/basic_factory.hpp>
#include <fifi/fifi_utils.hpp>

namespace kodo
{
    /// @todo Should we rename this to uniform_circular_generator
    ///
    /// @ingroup coefficient_generator_layers
    ///
    /// @brief Generates an uniform random coefficient (from the chosen
    ///        Finite Field) for every symbol.
    ///
    /// The circular_generator will use a structure similar to a circular
    /// buffer to generate coding coefficients for a random linear
    /// encoder/decoder.
    ///
    /// Fundamentals: The basic purpose of a generator is to generate the
    ///               coding coefficients representing an encoded symbol.
    ///
    /// So lets say we are encoding three symbols then we need three coding
    /// coefficients. The coefficient buffer would look something like:
    ///
    /// @code
    ///
    ///    +--+--+
    ///    |c1|c2|
    ///    +--+--+
    ///
    /// @endcode
    ///
    /// The standard generators will typically generate all coefficients
    /// everytime the generator gets invoked.
    ///
    /// The idea with the circular generator is the following:
    ///
    ///    1) Create an "expanded" coefficient buffer twice the needed
    ///       size.
    ///
    ///    2) Upon initialization fill the expanded buffer with
    ///       random coefficients.
    ///
    ///    3) Initialize a copy position to the start of the expanded buffer.
    ///
    ///    4) Every time the generator gets invoked move the copy position
    ///       one forward and generate a new random coefficient trailing
    ///       it. A special case exists when the copy position "wraps" (see
    ///       step 3 and step 4 below). In that case
    ///
    /// What about the "width" of the random number generator. There are
    /// two cases:
    ///
    ///    1) The width is larger or equal to the coefficient buffer. In
    ///       this case the circular generator does not make much sense,
    ///       since you can generate all coefficients in one go anyway.
    ///
    ///    2) The width is smaller than the coefficient buffers. In this
    ///       case we want to exploit our circular generator.
    ///
    /// If the width of the generator is larger than the size of the
    /// coefficient buffer we just generate all coefficients all the time.
    ///
    /// Lets say width is 4 bytes and buffer is 9 bytes.
    ///
    /// Then we make the expanded buffer 4 * 9
    ///
    /// @code
    ///
    /// Step 1:
    ///
    ///            +--+--+--+--+
    ///            |c1|c2|c3|c4|
    ///            +--+--+--+--+
    ///            ^     ^
    ///    copy +--+-----+
    ///
    /// Step 2:
    ///
    ///    rng +---+--+
    ///            v  v
    ///            +--+--+--+--+
    ///            |c1|c2|c3|c4|
    ///            +--+--+--+--+
    ///               ^     ^
    ///    copy +-----+-----+
    ///
    /// Step 3:
    ///
    ///    rng +------+--+
    ///               v  v
    ///            +--+--+--+--+
    ///            |c1|c2|c3|c4|
    ///            +--+--+--+--+
    ///                  ^     ^
    ///    copy +--------+-----+
    ///
    /// Step 4:
    ///
    ///    rng +---------+-----+
    ///                  v     v
    ///            +--+--+--+--+
    ///            |c1|c2|c3|c4|
    ///            +--+--+--+--+
    ///            ^     ^
    ///    copy +--+-----+
    ///
    /// Step 5:
    ///
    ///    rng +---+--+
    ///            v  v
    ///            +--+--+--+--+
    ///            |c1|c2|c3|c4|
    ///            +--+--+--+--+
    ///               ^     ^
    ///    copy +-----+-----+
    ///
    /// @endcode
    ///
    ///
    /// The "paddle" generator.
    /// The "circular" generator.
    /// The "Triangular" generator.
    ///
    /// Lets think about the cost of the different generators. We will
    /// define cost as the number of times we need to invoked the random
    /// number generator.
    ///
    /// Triangular:
    ///
    ///     Length of the
    ///
    /// The standard generator will
    ///

    template
    <
        class RandomNumberGenerator,
        class ValueType,
        class SuperCoder
    >
    class circular_generator : public SuperCoder
    {
    public:

        /// @copydoc layer::value_type
        typedef typename SuperCoder::field_type field_type;

        /// @copydoc layer::value_type
        typedef typename SuperCoder::value_type value_type;

        /// The random generator used
        typedef RandomNumberGenerator generator_type;

        /// @copydoc layer::seed_type
        typedef typename generator_type::result_type seed_type;

    public:

        /// Constructor
        circular_generator()
            : m_distribution(),
              m_super_value_distribution(field_type::min_value,
                                         field_type::max_value)
        { }

        /// @copydoc layer::initialize(Factory&)
        template<class Factory>
        void initialize(Factory& the_factory)
        {
            SuperCoder::initialize(the_factory);
            size = the_factory.max_coefficient_vector_size();
            if(SuperCoder::coefficient_vector_size() >= sizeof(ValueType))
            {
                vector_size_less_value_type_size = false;
            }
            generate_first_random_numbers();
        }

        /// @copydoc layer::generate(uint8_t*)
        void generate(uint8_t *coefficients)
        {
            assert(coefficients != 0);
            if(!vector_size_less_value_type_size)
            {
                temp_ptr = static_cast<void*>(coefficients);
                castet_coefficient_ptr = static_cast<ValueType*>(temp_ptr);
                uint32_t upper_bound = vector_size / 2;
                for(uint32_t i = 0; i < upper_bound; ++i)
                {
                    castet_coefficient_ptr[i] =
                        value_types[(next_pos_value_type + i) % vector_size];
                }
                value_types[next_pos_value_type] =
                    m_value_distribution(m_random_generator);
                next_pos_value_type = (next_pos_value_type + 1) % vector_size;
                if(uint8s_size)
                {
                    uint32_t starting_point = upper_bound * sizeof(ValueType);
                    for(uint32_t i = starting_point; i < size; ++i)
                    {
                        coefficients[i] =
                            uint8s[(next_pos_uint8 + i) % uint8s_size];
                        uint8s[next_pos_uint8] =
                            m_distribution(m_random_generator);
                        next_pos_uint8 = (next_pos_uint8 + 1) % uint8s_size;
                    }
                }
            }
            else
            {
                for(uint32_t i = 0; i < size; ++i)
                {
                    coefficients[i] =
                        uint8s[(next_pos_uint8 + i) % uint8s.size()];
                }
                uint8s[next_pos_uint8] =
                    m_distribution(m_random_generator);
                next_pos_uint8 = (next_pos_uint8 + 1) % uint8s.size();
            }
        }

        /// @copydoc layer::generate_partial(uint8_t*)
        void generate_partial(uint8_t *coefficients)
        {
            assert(coefficients != 0);
            std::fill_n(coefficients, SuperCoder::coefficient_vector_size(), 0);
            value_type *c = reinterpret_cast<value_type*>(coefficients);
            uint32_t symbols = SuperCoder::symbols();
            for(uint32_t i = 0; i < symbols; ++i)
            {
                if(!SuperCoder::can_generate(i))
                {
                    continue;
                }
                value_type coefficient =
                    m_super_value_distribution(m_random_generator);
                fifi::set_value<field_type>(c, i, coefficient);
            }
        }

        /// @copydoc layer::seed(seed_type)
        void seed(seed_type seed_value)
        {
            m_random_generator.seed(seed_value);
        }

    private:
    //Generates the random numbers random numbers for the coefficient vector.
    // These numbers are of size uint8_t and/or ValueType.
        void generate_first_random_numbers()
        {
            if(vector_size_less_value_type_size)
            {
                uint8s.resize(SuperCoder::coefficient_vector_size() * 2);
                uint8s_size = uint8s.size();
                for(uint32_t i = 0; i < uint8s_size; ++i)
                {
                    uint8s[i] =
                        m_distribution(m_random_generator);
                }
            }
            else
            {
                vector_size = (size / sizeof(ValueType))* 2;
                value_types.resize(vector_size);
                uint8s.resize((size % sizeof(ValueType)) * 2);
                uint8s_size = uint8s.size();
                for(uint32_t i = 0; i < vector_size; ++i)
                {
                    value_types[i] =
                        m_value_distribution(m_random_generator);
                }
                for(uint32_t i = 0; i < uint8s_size; ++i)
                {
                    uint8s[i] =
                        m_distribution(m_random_generator);
                }
            }
        }

    private:
    // Boolean set acording the coefficient vector size
        bool vector_size_less_value_type_size = true;

    // The size of the value_type vector, the uint8 vector and the
    // coefficient vector respectively.
        uint32_t vector_size;
        uint32_t uint8s_size;
        uint32_t size;

    private:

        std::vector<ValueType> value_types;
        std::vector<uint8_t> uint8s;


        void* temp_ptr;
        ValueType* castet_coefficient_ptr;
        uint32_t next_pos_value_type = 0;
        uint32_t next_pos_uint8 = 0;


    /// The type of the uint8_t distribution
        typedef boost::random::uniform_int_distribution<uint8_t>
        uint8_t_distribution;

    /// Distribution that generates random bytes
        uint8_t_distribution m_distribution;

    /// The type of the value_type distribution
        typedef boost::random::uniform_int_distribution<value_type>
        SuperCoder_value_type_distribution;

    /// Distribution that generates random values from a finite field
        SuperCoder_value_type_distribution m_super_value_distribution;

    /// The type of the value_type distribution
        typedef boost::random::uniform_int_distribution<ValueType>
        value_type_distribution;

    /// Distribution that generates random values from a finite field
        value_type_distribution m_value_distribution;

    /// The random generator
        RandomNumberGenerator m_random_generator;
    };
}
