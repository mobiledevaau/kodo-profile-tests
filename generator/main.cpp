// Copyright Steinwurf ApS 2015.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

/// This benchmark measure the average time spend to generate a coding vector
/// using different algorithms to generate coding coefficients.

#include <cstdint>
#include <random>

#include <gauge/gauge.hpp>
#include <gauge/console_printer.hpp>

#include <kodo/uniform_generator.hpp>
#include <kodo/circular_generator.hpp>
#include <kodo/raw_generic_generator.hpp>
#include <kodo/sparse_uniform_generator.hpp>
#include <kodo/sparse_uniform_index_generator.hpp>
#include <kodo/rebind_factory.hpp>
#include <kodo/storage_block_info.hpp>
#include <kodo/nonzero_generator.hpp>
#include <kodo/coefficient_info.hpp>
#include <kodo/finite_field_info.hpp>
#include <kodo/final_layer.hpp>

#include <boost/random/mersenne_twister.hpp> //mt11213b and mt19937

namespace
{
    // Small layer for "simulating" the rank of a codec stack. This is
    // mostly used to benchmark the layer::partial_generate(uint8_t*)
    // function.
    template<class SuperCoder>
    class generator_set_rank : public SuperCoder
    {
    public:

        /// Initialize the layer to default have full rank
        template<class Factory>
        void initialize(Factory& the_factory)
        {
            SuperCoder::initialize(the_factory);
            m_rank = the_factory.symbols();
        }

        /// @return the rank
        uint32_t rank()
        {
            return m_rank;
        }

        /// @param set the rank of stack
        void set_rank(uint32_t rank)
        {
            m_rank = rank;
        }

    protected:

        /// Keeps track of the rank of the stack
        uint32_t m_rank;
    };

    /// The purpose of this stack is to provide the API that a generator
    /// layer expects. This means that we can "wrap" a generator around
    /// this stack when we want to benchmark it.
    template<class Field>
    class base_stack : public
        generator_set_rank<
        kodo::coefficient_info<
        kodo::storage_block_info<
        kodo::finite_field_info<Field,
        kodo::final_layer
        >>>>
    {
    public:
        using factory = kodo::pool_factory<base_stack>;
    };

    /// This layer "wraps" a generator layer around the base stack.
    ///
    /// Example:
    ///
    ///    Lets say we have a generator called caffeine_generator with the
    ///    following definition:
    ///
    ///    template<class SuperCoder>
    ///    class caffeine_generator : public SuperCoder
    ///    {
    ///    public:
    ///
    ///        // ... here goes the implementation
    ///    };
    ///
    ///    The wrap_generator_stack will make the base_stack the SuperCoder
    ///    and rebind the factory to build types of:
    ///
    ///    caffeine_generator<base_stack<Field>>
    ///
    /// Usage:
    ///
    ///    To use it you can do the following:
    ///
    ///    template<class Field>
    ///    using caffeine_stack =
    ///        wrap_generator_stack<Field, caffeine_generator>;
    ///
    ///    And then somewhere in the code:
    ///
    ///    caffeine_stack<fifi::binary8>::factory factory(10,10);
    ///    auto stack = factory.build();
    ///
    template<class Field, template <class> class GeneratorLayer>
    class wrap_generator_stack : public GeneratorLayer<base_stack<Field>>
    {
    public:

        /// Rebind the factory to build the correct stack
        using factory = kodo::rebind_factory<
            base_stack<Field>, wrap_generator_stack>;
    };

    //Stack for the raw generator
    template<class Field, class RandomNumberGenerator,
        template <class, class> class GeneratorLayer>
    class wrap_generic_generator_stack : public GeneratorLayer<
        RandomNumberGenerator, base_stack<Field>>
    {
    public:

        /// Rebind the factory to build the correct stack
        using factory = kodo::rebind_factory<
            base_stack<Field>, wrap_generic_generator_stack>;
    };

    //Stack for the circular generator with uint8 as ValueType.

    template<class Field, class RandomNumberGenerator,
        template <class, class, class> class GeneratorLayer>
    class wrap_circular_uint8_generator_stack : public GeneratorLayer<
        RandomNumberGenerator, uint8_t, base_stack<Field>>
    {
    public:
        /// Rebind the factory to build the correct stack
        using factory = kodo::rebind_factory<
            base_stack<Field>, wrap_circular_uint8_generator_stack>;
    };

    //Stack for the circular generator with uint16 as ValueType.
    template<class Field, class RandomNumberGenerator,
        template <class, class, class> class GeneratorLayer>
    class wrap_circular_uint16_generator_stack : public GeneratorLayer<
        RandomNumberGenerator, uint16_t, base_stack<Field>>
    {
    public:
        /// Rebind the factory to build the correct stack
        using factory = kodo::rebind_factory<
            base_stack<Field>, wrap_circular_uint16_generator_stack>;
    };

    //Stack for the circular generator with uint32 as ValueType.
    template<class Field, class RandomNumberGenerator,
        template <class, class, class> class GeneratorLayer>
    class wrap_circular_uint32_generator_stack : public GeneratorLayer<
        RandomNumberGenerator, uint32_t, base_stack<Field>>
    {
    public:
        /// Rebind the factory to build the correct stack
        using factory = kodo::rebind_factory<
            base_stack<Field>, wrap_circular_uint32_generator_stack>;
    };


    /// Base class for a generator benchmark
    template<class GeneratorStack>
    class generator_benchmark  : public gauge::time_benchmark
    {
    public:

        using generator_factory_type = typename GeneratorStack::factory;

        using generator_type = typename generator_factory_type::pointer;

    public:

        void get_options(gauge::po::variables_map& options)
        {
            auto symbols = options["symbols"].as<std::vector<uint32_t> >();

            assert(symbols.size() > 0);

            for(auto s: symbols)
            {
                gauge::config_set cs;
                cs.set_value<uint32_t>("symbols", s);
                add_configuration(cs);
            }
        }

        void setup()
        {
            gauge::config_set config_set =
                gauge::time_benchmark::get_current_configuration();

            auto symbols = config_set.get_value<uint32_t>("symbols");

            generator_factory_type factory(symbols,10);
            m_generator = factory.build();

            assert(m_generator->coefficient_vector_size() > 0);
            m_buffer.resize(m_generator->coefficient_vector_size());
        }

        void test_body() final
        {
            assert(m_generator);
            assert(m_buffer.size() == m_generator->coefficient_vector_size());

            RUN
            {
                m_generator->generate(m_buffer.data());
            }
        }

    protected:

        // The buffer
        std::vector<uint8_t> m_buffer;

        // The coding coefficient vector generator
        generator_type m_generator;

    };

    /// Sparse generator benchmark
    template<class GeneratorStack>
    class sparse_generator_benchmark :
        public generator_benchmark<GeneratorStack>
    {
    public:

        typedef generator_benchmark<GeneratorStack> Super;

    public:

        void get_options(gauge::po::variables_map& options)
        {
            auto symbols = options["symbols"].as<std::vector<uint32_t> >();
            auto density = options["density"].as<std::vector<double> >();

            assert(symbols.size() > 0);
            assert(density.size() > 0);

            for(auto s: symbols)
            {
                for(auto d: density)
                {
                    gauge::config_set cs;
                    cs.set_value<uint32_t>("symbols", s);
                    cs.set_value<double>("density", d);
                    Super::add_configuration(cs);
                }
            }
        }

        void setup()
        {
            Super::setup();

            gauge::config_set config_set =
                gauge::time_benchmark::get_current_configuration();

            auto density = config_set.get_value<double>("density");

            m_generator->set_density(density);
        }

    protected:

        using Super::m_buffer;

        using Super::m_generator;

    };

}

BENCHMARK_OPTION(buffer_size)
{
    gauge::po::options_description options;

    options.add_options()
        ("symbols",
         gauge::po::value<std::vector<uint32_t>>()->
            default_value({16, 32, 64, 128}, "")->
            multitoken(),
         "The number of symbols in a generation");

    options.add_options()
        ("density",
         gauge::po::value<std::vector<double>>()->
            default_value({0.1, 0.2, 0.3, 0.4}, "")->
            multitoken(),
         "Average denisty of the coding vector");

    gauge::runner::instance().register_options(options);
}


////////////////////////////////////////////////////////////
// kodo::uniform_generator
////////////////////////////////////////////////////////////

template<class Field>
using uniform_stack = wrap_generator_stack<Field, kodo::uniform_generator>;

template<class Field>
using uniform_benchmark = generator_benchmark<uniform_stack<Field>>;

BENCHMARK_F(uniform_benchmark<fifi::binary>, uniform, binary, 10);
BENCHMARK_F(uniform_benchmark<fifi::binary8>, uniform, binary8, 10);

////////////////////////////////////////////////////////////
// kodo::raw_generaric_generator with mt11213b
////////////////////////////////////////////////////////////
template<class Field>
using raw_mt11213b_stack = wrap_generic_generator_stack<Field, boost::mt11213b,
    kodo::raw_generic_generator>;

template<class Field>
using raw_mt11213b_benchmark = generator_benchmark<raw_mt11213b_stack<Field>>;

BENCHMARK_F(raw_mt11213b_benchmark<fifi::binary>, raw_mt11213b, binary, 10);
BENCHMARK_F(raw_mt11213b_benchmark<fifi::binary8>, raw_mt11213b, binary8, 10);

////////////////////////////////////////////////////////////
// kodo::raw_generaric_generator with mt19937
////////////////////////////////////////////////////////////
template<class Field>
using raw_mt19937_stack = wrap_generic_generator_stack<Field, boost::mt19937,
    kodo::raw_generic_generator>;

template<class Field>
using raw_mt19937_benchmark = generator_benchmark<raw_mt19937_stack<Field>>;

BENCHMARK_F(raw_mt19937_benchmark<fifi::binary>, raw_mt19937, binary, 10);
BENCHMARK_F(raw_mt19937_benchmark<fifi::binary8>, raw_mt19937, binary8, 10);

////////////////////////////////////////////////////////////
// kodo::circular_generator with mt11213b
////////////////////////////////////////////////////////////

// ValueType unit8
/*
template<class Field>
using circular_mt11213b_uint8 = wrap_circular_uint8_generator_stack<Field,
    boost::mt11213b, kodo::circular_generator>;

template<class Field>
using circular_benchmark_mt11213b_uint8 = generator_benchmark<
    circular_mt11213b_uint8<Field>>;

BENCHMARK_F(circular_benchmark_mt11213b_uint8<fifi::binary>, circular_uint8_mt11213b,
            binary, 10);
BENCHMARK_F(circular_benchmark_mt11213b_uint8<fifi::binary8>, circular_uint8_mt11213b,
            binary8, 10);
*/
// ValueType uint16
/*
template<class Field>
using circular_mt11213b_uint16 = wrap_circular_uint16_generator_stack<Field,
    boost::mt11213b, kodo::circular_generator>;

template<class Field>
using circular_benchmark_mt11213b_uint16 = generator_benchmark<
    circular_mt11213b_uint16<Field>>;

BENCHMARK_F(circular_benchmark_mt11213b_uint16<fifi::binary>, circular_uint16_mt11213b,
            binary, 10);
BENCHMARK_F(circular_benchmark_mt11213b_uint16<fifi::binary8>, circular_uint16_mt11213b,
            binary8, 10);
*/
// ValueType uint32
/*
template<class Field>
using circular_mt11213b_uint32 = wrap_circular_uint32_generator_stack<Field,
    boost::mt11213b, kodo::circular_generator>;

template<class Field>
using circular_benchmark_mt11213b_uint32 = generator_benchmark<
    circular_mt11213b_uint32<Field>>;

BENCHMARK_F(circular_benchmark_mt11213b_uint32<fifi::binary>, circular_uint32_mt11213b,
            binary, 10);
BENCHMARK_F(circular_benchmark_mt11213b_uint32<fifi::binary8>, circular_uint32_mt11213b,
            binary8, 10);
*/
////////////////////////////////////////////////////////////
// kodo::circular_generator with mt19937
////////////////////////////////////////////////////////////

// ValueType unit8
/*
template<class Field>
using circular_mt19937_uint8 = wrap_circular_uint8_generator_stack<Field,
    boost::mt19937, kodo::circular_generator>;

template<class Field>
using circular_benchmark_mt19937_uint8 = generator_benchmark<
    circular_mt19937_uint8<Field>>;

BENCHMARK_F(circular_benchmark_mt19937_uint8<fifi::binary>,
            circular_uint8_mt19937, binary, 10);
BENCHMARK_F(circular_benchmark_mt19937_uint8<fifi::binary8>,
            circular_uint8_mt19937, binary8, 10);
*/
// ValueType uint16
/*
template<class Field>
using circular_mt19937_uint16 = wrap_circular_uint16_generator_stack<Field,
    boost::mt19937, kodo::circular_generator>;

template<class Field>
using circular_benchmark_mt19937_uint16 = generator_benchmark<
    circular_mt19937_uint16<Field>>;

BENCHMARK_F(circular_benchmark_mt19937_uint16<fifi::binary>,
            circular_uint16_mt19937, binary, 10);
BENCHMARK_F(circular_benchmark_mt19937_uint16<fifi::binary8>,
            circular_uint16_mt19937, binary8, 10);
*/
// ValueType uint32
/*
template<class Field>
using circular_mt19937_uint32 = wrap_circular_uint32_generator_stack<Field,
    boost::mt19937, kodo::circular_generator>;

template<class Field>
using circular_benchmark_mt19937_uint32 = generator_benchmark<
    circular_mt19937_uint32<Field>>;

BENCHMARK_F(circular_benchmark_mt19937_uint32<fifi::binary>,
            circular_uint32_mt19937, binary, 10);
BENCHMARK_F(circular_benchmark_mt19937_uint32<fifi::binary8>,
            circular_uint32_mt19937, binary8, 10);
*/
////////////////////////////////////////////////////////////
// kodo::sparse_uniform_generator
////////////////////////////////////////////////////////////

template<class Field>
using sparse_uniform_stack =
    wrap_generator_stack<Field, kodo::sparse_uniform_generator>;

template<class Field>
using sparse_uniform_benchmark =
    sparse_generator_benchmark<sparse_uniform_stack<Field>>;

BENCHMARK_F(sparse_uniform_benchmark<fifi::binary>,
            sparse_uniform, binary, 10);

BENCHMARK_F(sparse_uniform_benchmark<fifi::binary8>,
            sparse_uniform, binary8, 10);

////////////////////////////////////////////////////////////
// kodo::sparse_uniform_generator with nonzero
////////////////////////////////////////////////////////////

template<class Field>
struct nonzero_sparse_uniform_stack :
    kodo::nonzero_generator<
    kodo::sparse_uniform_generator<
    base_stack<Field>>>
{
    /// Rebind the factory to build the correct stack
    using factory = kodo::rebind_factory<
        base_stack<Field>, nonzero_sparse_uniform_stack>;
};

template<class Field>
using nonzero_sparse_uniform_benchmark =
    sparse_generator_benchmark<nonzero_sparse_uniform_stack<Field>>;


BENCHMARK_F(nonzero_sparse_uniform_benchmark<fifi::binary>,
            nonzero_sparse_uniform, binary, 10);

BENCHMARK_F(nonzero_sparse_uniform_benchmark<fifi::binary8>,
            nonzero_sparse_uniform, binary8, 10);

////////////////////////////////////////////////////////////
// kodo::sparse_uniform_index_generator
////////////////////////////////////////////////////////////

template<class Field>
using sparse_uniform_index_stack =
    wrap_generator_stack<Field, kodo::sparse_uniform_index_generator>;

template<class Field>
using sparse_uniform_index_benchmark =
    sparse_generator_benchmark<sparse_uniform_index_stack<Field>>;

BENCHMARK_F(sparse_uniform_index_benchmark<fifi::binary>,
            sparse_uniform_index, binary, 10);

BENCHMARK_F(sparse_uniform_index_benchmark<fifi::binary8>,
            sparse_uniform_index, binary8, 10);

int main(int argc, const char* argv[])
{
    srand((uint32_t)time(0));

    gauge::runner::add_default_printers();
    gauge::runner::run_benchmarks(argc, argv);
    return 0;
}
