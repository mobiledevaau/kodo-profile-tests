// Copyright Steinwurf ApS 2011-2013.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#include <kodo/rlnc/full_vector_codes.hpp>
#include <stdio.h>
#include "timingtest.h"

int main(int argc, char* argv[])
{
    // Set the number of symbols (i.e. the generation size in RLNC
    // terminology) and the size of a symbol in bytes
    uint32_t max_symbols = atoi(argv[1]);
    uint32_t max_symbol_size = atoi(argv[2]);

    uint32_t encoder_count = atoi(argv[3]); //Default 10.000
    uint32_t encoded_symbols = max_symbols * 2;

    typedef kodo::rlnc::full_vector_encoder<fifi::binary8> rlnc_encoder;

    // In the following we will make an encoder/decoder factory.
    // The factories are used to build actual encoders/decoders
    rlnc_encoder::factory encoder_factory(max_symbols, max_symbol_size);

     std::string seed_type = static_cast<std::string>(argv[4]);

     if(seed_type.compare("both") == 0 || seed_type.compare("with") == 0)
     {
         encoder_factory.set_seed(0);
     }

    std::vector<uint8_t> payload(encoder_factory.max_payload_size());
    std::vector<uint8_t> block_in(encoder_factory.max_block_size());

    // Just for fun - fill the data with random data
    for(auto &e: block_in)
        e = rand() % 256;

    //Prepare PMCtest
    const uint32_t pmc_num = 0x40000001;
    const uint32_t pmc_cache = 0x00000001;
    uint32_t clockcounts[encoder_count];
    uint32_t pmccounts[encoder_count];
    uint32_t cachecounts[encoder_count];

    uint32_t clock1, clock2, pmc1, pmc2, cache1, cache2;

    for(uint32_t i = 0; i < encoder_count; ++i)
    {
        auto encoder = encoder_factory.build();
        encoder->set_systematic_off();
        encoder->set_symbols(sak::storage(block_in));

        serialize();
        clock1 = (uint32_t)readtsc();
        pmc1 = (uint32_t)readpmc(pmc_num);
        cache1 = (uint32_t)readpmc(pmc_cache);

        for(uint32_t j = 0; j < encoded_symbols; ++j)
        {
            encoder->write_payload(payload.data());
        }

        serialize();
        clock2 = (uint32_t)readtsc();
        pmc2 = (uint32_t)readpmc(pmc_num);
        cache2 = (uint32_t)readpmc(pmc_cache);

        clockcounts[i] = clock2 - clock1;
        pmccounts[i] = pmc2 - pmc1;
        cachecounts[i] = cache2 - cache1;
    }

    system("echo run,clocks,cache > out.json");

    char str[40];
    for(uint32_t i = 0; i < encoder_count; ++i){
        sprintf(str, "echo %d,%d,%d >> out.json", i+1, clockcounts[i], cachecounts[i]);
        system(str);
    }

    return 0;

}
