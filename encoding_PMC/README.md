# README #

Purpose
=====

This folder contains a files and a description of how to measure the Performance Measurement Counters (PMC) on ones CPU. There is an example of how to run it but one must correct the test to fit the current CPU. 

All work here in done by inspiration of the work of Agner Fogh whose work is used. His great work can be found [here](http://www.agner.org/optimize/#testp).
The folder *testp* contains all files that Agner Fogh supplies for the CPU test. It also includes changes that will make it easier to determine ones CPU.

The rest of the README contain explanations for Linux. If Windows is used one must read *testp.pdf* in the *testp* folder. 

Necessary installation
======================
In order to be able to used Agner's files one must do two installations.

```
sudo apt-get install g++-multilib
sudo apt-get install nasm
```

*testp.pdf* contains a deeper explanation if needed.

Driver
=====
In order to make it possible to read the PMC on the CPU a driver must be installed. This driver must be installed after each reboot. Otherwise an exception is thrown if one tries to read the counters. 

To install the driver unzip the *DriverSrcLinuz.zip* and then type:
```
chmod a+x *.sh
make
sudo ./install.sh
```

The last command must be run after reboot if one must read the counters again.

Structure of test
=================
Agner Fogh gives two ways to measure a piece of code. Either the code is included in the *PMCTestA.cpp* that he provides of one must include function calls for reads in the code to be tested. 
The latter was choosen for this test since a lot of preperation is needed a large list of dependencies is needed. For this test the encoding is tested. 
What is tested depends on the CPU used if one don't know how to modify Agner's files to include more options for PMCs. 

The basic structure of the test is:
```
Setup

Read PMC to get starting point

Code to test

Read PMC again

Find the difference

Save difference as result
```

How to see CPU type
------------------
In the *testp* folder there is a folder named *PMCTest*. In this folder a there are two make shell script, c32.sh and c64.sh. Run the shell script that fits the current architecture. 
*ToDo Include example*

In order to see the family of the CPU look in the source file *PMCTestA.cpp* on line 535. In this switch case one can see which processor family it processor is in. 

Starting the PMCs
------------------

How to change PMCs to measure in the code to test
-----------------------------
In *encoding_benchmark.cpp* PMC values are read by the `readpmc(int pcm_num)` function. The value here can be any value given when preparing the PMCs. 
It is therefore recommended that the previous step is done before anything is changed in the source files. 

How to build test
----------------