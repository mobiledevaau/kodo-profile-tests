import argparse


def _arg_symbol_size_start_(parser):
    parser.add_argument('--symbol_size_start',
                        help='The starting value of max_symbol_size.',
                        default=1,
                        type=int)

def _arg_symbol_size_step_(parser):
    parser.add_argument('--symbol_size_step',
                        help='The size of the steps when choosing next max_symbol_size.',
                        default=1,
                        type=int)

def _arg_symbol_size_upper_(parser):
    parser.add_argument('--symbol_size_upper',
                        help='The upper bound of max_symbol_size',
                        default=1500,
                        type=int)

def _arg_generation_start_(parser):
    parser.add_argument('--generation_start',
                        help='The starting value of max_symbols',
                        default=1,
                        type=int)

def _arg_generation_step_(parser):
    parser.add_argument('--generation_step',
                        help='The size of the steps when choosing next max_symbols.',
                        default=1,
                        type=int)

def _arg_generation_upper_(parser):
    parser.add_argument('--generation_upper',
                        help='The upper bound of max_symbols',
                        default=1500,
                        type=int)

def _arg_opperation_(parser):
    parser.add_argument('-o','--operation',
                        help='Sets the type(s) of operation(s)',
                        default='both',
                        choices=['both', 'measure', 'pdf'])

def _arg_folder_(parser):
    parser.add_argument('--prof_folder',
                        help='Sets the relevative path for the folder containing the prof results',
                        default='results')

def _arg_runs_(parser):
    parser.add_argument('--runs',
                        help='The number of runs the benchmarks have with a single setting.',
                        default=10000,
                        type=int)

def _arg_pdf_name_(parser):
    parser.add_argument('--pdf_name',
                        help='The name of the pdf containing the results. Default is the name of the folder containing *.prof files')


def _arg_plots_pp_(parser):
    parser.add_argument('--plots_pp',
                        help='Number of plots in the pdf. Default is to print all possible.',
                        type=int)

def _arg_binary_order_(parser):
    parser.add_argument('--binary_order',
                        help='Sets the binary field. Default is 2.',
                        nargs='*',
                        choices=['2', '8', '16'],
                        default=['2'],
                        type=int)

def _arg_seed_(parser):
    parser.add_argument('-s', '--seed',
                        help='whether or not the random number generator is seeded',
                        nargs='*',
                        default=['with', 'without'],
                        choices=['with', 'without'])


def benchmark_arg_parser(parser):

    _arg_symbol_size_start_(parser)
    _arg_symbol_size_step_(parser)
    _arg_symbol_size_upper_(parser)
    _arg_generation_start_(parser)
    _arg_generation_step_(parser)
    _arg_generation_upper_(parser)
    _arg_opperation_(parser)
    _arg_folder_(parser)
    _arg_runs_(parser)
    _arg_pdf_name_(parser)
    _arg_plots_pp_(parser)
    _arg_binary_order_(parser)
    _arg_seed_(parser)

    return parser
