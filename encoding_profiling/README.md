# README #

Purpose of Encoder Profiler.
=====
This folder contains test for the profiler benchmarks for the encoder implemented by kodo. The benchmark was designed on the idea that the Pseudo Random Number Generator (PRNG) took most of the total time the encoder used. Google perftool is the tool used to profile. The executable must be compiled with debug symbols in order to see which functions the time is used in.

This README also contains explanations on how to understand the results produced by google perftool.


Build the benchmark
=====
To configure the test by running `waf` found is the folder:

```
#!python
python waf configure
```

This will configure the project and download all the dependencies needed.

After configure run the following command to build:

```
#!python
python waf build --options=cxx_debug
```


If you experience problems with either of the two previous steps you can
check the Kodo manual's `Getting Started`  section for trubleshooting.

[Getting Started](https://kodo.readthedocs.org/en/latest/getting_started.html)

Google Perftools
===============
The tool used for profiling Kodo was Google Perftools which are a collection of tools to analyze C++ programs. The tool that have been used here is the CPU profiling tool. A short guide is presented in how it was used. The complete package for Google Perftools can be installed by typing

```
sudo apt-get install google-perftools
```

The libary used to make the CPU profiling can either be added at compile time or added to the executeable binary file which we want to analyze. Here it has been added to the executeable binary file. When Google Perftools have been installed there should be a file " libprofiler.so.0" at
```
/usr/lib/
```

If this is not the case the file can be located by
```
apt-file search libprofiler.so.0
```

Remember the location of the `libprofiler` since it is used for execution.

Run
===
Preperation
----------
The folder contains a `run_benchmark.py` that runs the encoding profiling. At the momont the location of the `libprofiler` is hardcoded. If and only if the location is anythin other than `/usr/lib/libprofiler.so.0` is location of the file must be correted in the `_run_benchmark_` function in the `sp.call` call.

More information of google perftools' CPU profiler kan be found [here](http://google-perftools.googlecode.com/svn/trunk/doc/cpuprofile.html) if needed.

Execute
-------

To run the default tests type

```
python run_benchmarks.py
```

You can run the script with the ``--help`` option to see which parameters are supported:
```
python run_benchmarks.py --help
```

Results
=======
Whenever at setting for the encoder benchmark is run a corresponding `.prof` file is produced in a `Results` folder. If no name for a folder for the prof files is given the folder is name `Results`.

If `run_benchmark.py` is run with the option `operation` as `both` or `pdf` an pdf output is produced that plots the results. The `both` setting is the default.

Each plot illustrated how much time is used in seeding the PRNG, in generation the random number, in finite field mathematics and in all other functions. One plot shows all tried symbol sizes for a given generation. The results for whether or not a seed is used is split up in different pdf files. All pdf files are named according to the folder the prof files are in and appended with any usefull specifiers and placed in the `Results` folder.