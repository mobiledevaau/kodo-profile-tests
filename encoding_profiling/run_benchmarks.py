#! /usr/bin/env python

######################### Purpose ###############################
# This script is used for profiling the encoder benchmark of
# kodo and to plot the results. Is written to sweet a range of
# values for symbols and symbol size.
#
######################### Execution ############################
# When ecexuted the pwd must be the folder where the source
# files for the encoder_benchmark. Another thing is waf must
# have been run with options=cxx_debug set. The reason is that
# _run_benchmark_ finds the executable in the build folder and
# the debug symbols are needed to get any usefull results.
#
# One must have google perftools install in order to run!
# Read README.md for more info.
###############################################################

import argparse
import re
import shutil
import sys
import os
import subprocess as sp
from os.path import basename
from tempfile import mkstemp

#For the pdf creation.
from pandas import *
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

# Import of the custom command line args.
sys.path.append(os.getcwd())
import arg_parsing

# Function for running the encode benchmark and creating *.prof files.
# Is designed for sweeping over a range of values. Function creates a result
# folder if it is not there.
def _run_benchmark_(args):

    folder = 'results/' + args.prof_folder
    if not os.path.exists(os.getcwd() + '/' +  folder):
        os.makedirs(os.getcwd() + '/' + folder)


    x = args.generation_start
    y = args.symbol_size_start

    seeds = ['with', 'without']
    binary_orders = []

    if args.seed == 'with' or args.seed == 'without':
        seeds.remove(args.seed)

    sp.call(["./waf"])
    while x <=args.generation_upper:
        while y <= args.symbol_size_upper:
            for seed in seeds:
               # print type(args.binary_order)
                for binary_order in args.binary_order:
                    # Print function. Good for long runs to se how far one are.
                    print('stated with generation ' + str(x)
                          + ', symbols size ' + str(y)
                          + ', ' + seed + ' seed'
                          + ' and binary order ' + str(binary_order))

                    sp.call(["LD_PRELOAD=/usr/lib/libprofiler.so.0 "
                             + " CPUPROFILE=out.prof "
                             + "./build/linux_debug/encode_benchmark "
                             + str(x) + " " + str(y) + " " + str(args.runs)
                             + " " + seed
                             + " " + str(binary_order)]
                            ,shell=True)

                    shutil.move(
                        'out.prof',
                        folder + '/' + str(x) + '_'
                        + str(y) + '_' + seed + '_binary'
                        + str(binary_order) +  '.prof')

            y = y + args.symbol_size_step
        y = args.symbol_size_start
        x = x + args.generation_step


# Creates two csv files from the *.prof files.
# The files are named according to the seed type.
def _run_csv_(args):
    for seed in args.seed:
        for binary in args.binary_order:
            csv_file = 'results_' + seed + '_binary' + str(binary) + '.csv'
            new_file = open(csv_file, 'w')
            new_file.write('Gen_size,max_symbol_size,time_generator,time_fifi,time_seeding\n')

            files = os.listdir('results/' + args.prof_folder)
            folder = 'results/' + args.prof_folder

            regex_file = re.compile( seed + '_binary' + str(binary) + '.prof')
            regex_1 = re.compile('[\d|\s]\d.\d(?=% kodo::uniform_generator::generate)')
            regex_2 = re.compile('[\d|\s]\d.\d(?=% kodo::finite_field_math)')
            regex_3 = re.compile('[\d|\s]\d.\d(?=% kodo::seed_generator)')

            seed_files = []

            for data_file in files:
                if regex_file.search(basename(data_file)):
                    seed_files.append(data_file)

            # Uses regex to find the the data to include in the csv.
            for data_file in seed_files:
                call_string = 'google-pprof --text build/linux_debug/encode_benchmark ' + folder + '/' + basename(data_file) + ' > tmp.txt'
                sp.call([call_string], shell=True)

                prof = 'tmp.txt'
                prof_file = open(prof, 'r')
                dat_1 = '0'
                dat_2 = '0'
                for f in prof_file:
                    if regex_1.search(f):
                        dat_1 = regex_1.search(f).group()
                    if regex_2.search(f):
                        dat_2 = regex_2.search(f).group()
                    if regex_3.search(f):
                        dat_3 = regex_3.search(f).group()

                gen = basename(data_file).split('.')[0].split('_')[0]
                max_size = basename(data_file).split('.')[0].split('_')[1]
                new_file.write(gen + ',' + max_size  + ',' + dat_1 + ',' + dat_2 + ',' + dat_3 + '\n')
                prof_file.close()
                os.remove('tmp.txt')

            new_file.close()



#Creates a pdf of one plot per page.
def _run_plot_(args):
    #Prepares the DataFrame
    for seed in args.seed:
        for binary in args.binary_order:
            df = read_csv('results_' + seed + '_binary' + str(binary) + '.csv').set_index(
                ['Gen_size',
                 'max_symbol_size']
            ).sort_index()

            df['other'] = Series(0.0, index=df.index)
            df['other'] = 100.0 - df['time_fifi'] - df['time_generator']

            #Used values for generation size
            generation_values = df.index.values.tolist()
            generation_values = list(set([i[0] for i in generation_values]))
            generation_values.sort()

            if not args.pdf_name:
                pdf ='results/' + args.prof_folder + '_' + seed + '_seed_binary' + str(binary) + '.pdf'
            else:
                pdf ='results/' + args.pdf_name + '_' + seed + '_seed_binary' + str(binary) +'.pdf'

            with PdfPages(pdf) as pdf:
                print 'Creating PDF of one plot per page for seed setting ' + seed + ' and binary ' + str(binary)
                for i in generation_values:
                    ax = df.loc[i].plot(
                        kind='bar',
                        stacked=True,
                        title = 'Generation size ' + str(i), figsize=(8,8))
                    ax.set_ylim(0,100)
                    ax.set_ylabel('Time used of total')
                    pdf.savefig()
                    plt.close()

#Creates a pdf of two plots per page.
def _run_plot_2_(args):
     #Prepares the DataFrame
    for seed in args.seed:
        for binary in args.binary_order:
            df = read_csv('results_' + seed + '_binary' + str(binary) + '.csv').set_index(
                ['Gen_size',
                 'max_symbol_size']
            ).sort_index()

            df['other'] = Series(0.0, index=df.index)
            df['other'] = 100.0 - df['time_fifi'] - df['time_generator']

            #Used values for generation size
            generation_values = df.index.values.tolist()
            generation_values = list(set([i[0] for i in generation_values]))
            generation_values.sort()
            generation_values = iter(generation_values)

            if not args.pdf_name:
                pdf ='results/' + args.prof_folder + '_'+ seed + '_seed__binary' + str(binary) + '_2pp.pdf'
            else:
                pdf ='results/' + args.pdf_name + '_'+ seed + '_seed__binary' + str(binary) + '_2pp.pdf'

            # If an uneven number of plots appears an exception is thrown.
            # This exception is handlende bu plotting the last plot alone.
            with PdfPages(pdf) as pdf:
                print 'Creating PDF of two plot per page for seed setting ' + seed + ' and binary ' + str(binary)
                for i in generation_values:

                    try:
                        i_next = next(generation_values)
                        fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(12, 8), squeeze=False)
                    except StopIteration:
                        i_next = None
                        fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(12, 8), squeeze=False)

                    ax = df.loc[i].plot(
                        ax=axes[0,0],
                        kind='bar',
                        stacked=True,
                        title = 'Generation size ' + str(i),
                        sharex=False)
                    ax.set_ylim(0,100)
                    ax.set_ylabel('Time used of total')

                    if not i_next is None:
                        ax = df.loc[i_next].plot(
                            ax=axes[1,0],
                            kind='bar',
                            stacked=True,
                            title = 'Generation size ' + str(i_next),
                            sharex=False)
                        ax.set_ylim(0,100)
                        ax.set_ylabel('Time used of total')
                        plt.tight_layout()

                    pdf.savefig()
                    plt.close()

def main():

    parser = argparse.ArgumentParser()
    parser = arg_parsing.benchmark_arg_parser(parser)
    args = parser.parse_args()

    if args.operation == 'both' or args.operation == 'measure':
        _run_benchmark_(args)
    if args.operation == 'both' or args.operation == 'pdf':
        _run_csv_(args)
        if args.plots_pp == 1 or not args.plots_pp:
            _run_plot_(args)
        if args.plots_pp == 2 or not args.plots_pp:
            _run_plot_2_(args)

if __name__ == '__main__':
    main()
