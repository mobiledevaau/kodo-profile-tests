// Copyright Steinwurf ApS 2011-2013.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#include <kodo/rlnc/full_vector_codes.hpp>

template<class binaryType>
void encoder_benchmark(
    uint32_t max_symbols,
    uint32_t max_symbol_size,
    uint32_t encoder_count,
    std::string seed_type){

    uint32_t encoded_symbols = max_symbols * 2;

    typedef kodo::rlnc::full_vector_encoder<binaryType> rlnc_encoder;
    // In the following we will make an encoder/decoder factory.
    // The factories are used to build actual encoders/decoders
    typename rlnc_encoder::factory encoder_factory(max_symbols, max_symbol_size);

    if(seed_type.compare("both") == 0 || seed_type.compare("with") == 0)
    {
        encoder_factory.set_seed(0);
    }

    std::vector<uint8_t> payload(encoder_factory.max_payload_size());
    std::vector<uint8_t> block_in(encoder_factory.max_block_size());

    // Just for fun - fill the data with random data
    for(auto &e: block_in){
        e = rand() % 256;
    }

    for(uint32_t i = 0; i < encoder_count; ++i)
    {
        auto encoder = encoder_factory.build();
        encoder->set_systematic_off();
        encoder->set_symbols(sak::storage(block_in));

        for(uint32_t j = 0; j < encoded_symbols; ++j)
        {
            encoder->write_payload(payload.data());
        }
    }
}

int main(int argc, char* argv[])
{
    // Set the number of symbols (i.e. the generation size in RLNC
    // terminology) and the size of a symbol in bytes
    uint32_t max_symbols = atoi(argv[1]);
    uint32_t max_symbol_size = atoi(argv[2]);

    uint32_t encoder_count = atoi(argv[3]); //Default 10.000
    std::string seed_type = static_cast<std::string>(argv[4]);

    uint32_t binary_order = atoi(argv[5]);

    if(binary_order == 2){
        encoder_benchmark<fifi::binary>(max_symbols, max_symbol_size, encoder_count, seed_type);
    }
    else if(binary_order == 8){
        encoder_benchmark<fifi::binary8>(max_symbols, max_symbol_size, encoder_count, seed_type);
    }
    else if(binary_order == 16){
        encoder_benchmark<fifi::binary16>(max_symbols, max_symbol_size, encoder_count, seed_type);
    }

    return 0;

}
